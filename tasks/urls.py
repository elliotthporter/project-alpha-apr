from django.urls import path
from .views import create_task, MyTasksView

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", MyTasksView.as_view(), name="show_my_tasks"),
]
